Thinushuan Chandirakumar & Jeffrey Santana                      LB2

**LB2 Docker**

**Server auswahl**

Wir haben uns für Ubuntu entschieden. Dieser Server soll in Virtualbox laufen. Wir haben uns dafür entschieden, weil wir gehört haben dass es besser funktionieren soll. Zudem 
haben wir das Wahlmodul 901 besucht und uns dort mit Linux angefreundet.

**Server einrichten**
Thinushan Chandirakumar
1. vagrant box add http://10.1.66.11/vagrant/ubuntu/xenial64.box --name ubuntu/xenial64
2. vagrant init ubuntu/xenial64-docker
3. vagrant up

**MySQL einrichten**
Jeffrey Santana
Datenbank für OwnCloud
1. mkdir docker
2. mkdir mysql
3. sudo nano dockerfile
4. "FROM mysql:8.0.3"
5. docker -build -t mysqljstc
6. docker run --name mysqljstc -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:8.0.3 --link mysqljstc:mysql
7. Zum Überprüfen docker ps ausführen


**MariaDB einrichten**¨
Thinushuan Chandirakumar
Datenbank für OwnCloud --> zum Ausstausch der Datenbank
1. mkdir mariadb
2. cd mariadb
3. sudo nano dockerfile
4. "FROM mariadb:10.0.3"
5. docker -build -t mariadb
6. docker run --name mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:10.0.3
7. Zum Überprüfen docker ps ausführen

**OwnCloud**
Jeffrey Santana
Apache wurde gewählt, da wir bisher gute Erfahrungen gemacht haben.
1. mkdir owncloud
2. cd owncloud
3. sudo nano dockerfile
4. "FROM owncloud:10.0.2-apache
5. EXPOSE 80"
5. docker -build -t owncloudone
6. docker run --name owncloudone  -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:8.0.3
7. Zum Überprüfen docker ps ausführen


**Auf exteren Speicher mit Volumes**
Mit dem Parameter -v Speicherort_Host:Mappingort kann man ein Verzeichnis direkt in den Container mappen.
Danach gibt es die zusätzlich noch die Möglichkeit mit dem Parameter COPY Speicherort_Host /OrtaufContainer

-----------------------------------------
13.09.2017                      

